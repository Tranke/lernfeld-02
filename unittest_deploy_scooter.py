"""Unittest für main.deploy_scooter()"""

import unittest
from main import *

class TestDeployScooter(unittest.TestCase):
    """Unittests für deploy_scooter(model, longi, lati)"""
    def test_deploy_scooter(self):
        querier = Model()
        model = 'Lite'
        longi = '20'
        lati = '50'
        id = main.deploy_scooter(model, longi, lati) # scooter id
        self.assertEqual("((%s, 'Lite', '20', '50', Decimal('100')),)" % id, str(querier.executeQuery("Select * from Scooter Where Scooter_ID LIKE %s" % id)))
        sql_delete ="DELETE FROM Scooter WHERE Scooter_ID = '%s'" % str(id)
        try:
            querier.executeQuery(sql_delete)
        except Exception as exc:
            print(exc)
if __name__ == '__main__':
    unittest.main()
