from tkinter import *
from tkinter import messagebox
from main import *

from Controller import *
from PIL import ImageTk, Image

Result = "0"
cont = Controller('User', 'Username', 'Username', 'RoGu')
querier = Model()


def switchToLoginView():
    BookingView.main_screen.withdraw()
    CalculationView.calculationScreen.withdraw()
    NavigationView.navigationScreen.withdraw()
    LoginView.screen.deiconify()


def switchToNavigationView():
    BookingView.main_screen.withdraw()
    CalculationView.calculationScreen.withdraw()
    NavigationView.navigationScreen.deiconify()
    CalculationView.fetchedResultsLabel = Label(CalculationView.calculationScreen, text="  0  ").grid(row=6, column=4)
    CalculationView.fetchedDurationLabel = Label(CalculationView.calculationScreen, text="  -  ").grid(row=5, column=2)
    CalculationView.fetchedPriceInEuroLabel = Label(CalculationView.calculationScreen, text="  -  ").grid(row=5,
                                                                                                          column=4)
    bookingIDEntry = Entry(CalculationView.calculationScreen, text="please enter booking ID")
    bookingIDEntry.grid(row=3, column=3)

def switchToNavigationView2():
    ScooterView.scooter_screen.withdraw()
    CalculationView.calculationScreen.withdraw()
    NavigationView.navigationScreen.deiconify()

def switchToBookingView():
    NavigationView.navigationScreen.withdraw()
    CalculationView.calculationScreen.withdraw()
    BookingView.main_screen.deiconify()


def switchToCalculationView():
    BookingView.main_screen.withdraw()
    NavigationView.navigationScreen.withdraw()
    CalculationView.calculationScreen.deiconify()

def switchToScooterView():
    BookingView.main_screen.withdraw()
    NavigationView.navigationScreen.withdraw()
    ScooterView.scooter_screen.deiconify()



def view_latest_bookings():
    sql_latest_bookings = "SELECT Booking_ID FROM Booking ORDER BY Booking_ID DESC LIMIT 10"
    result = str(querier.executeQuery(sql_latest_bookings))[1:-1]
    messagebox.showinfo("Latest bookings", "%s" %result, master=CalculationView.calculationScreen)


def forgot_password():
    LoginView.screen.withdraw()
    messagebox.showinfo("Forgot-Password", "You will recieve a new password via mail!", master=LoginView.screen)
    LoginView.screen.deiconify()


def deploy_scooter():
    entry_model = ScooterView.scooter_model_entry.get()
    entry_longitude = ScooterView.scooter_longitude_entry.get()
    entry_latitude = ScooterView.scooter_latitude_entry.get()
    number = main.deploy_scooter(entry_model, entry_longitude, entry_latitude)
    messagebox.showinfo("Scooter deployment", "Deployed Scooter [ID= %s]" % number, master=ScooterView.scooter_screen,)

def button_action():
    # this method is activated when pressing the login button and will check if your username and password is correct
    entryText = LoginView.username_entry.get()
    entryText2 = LoginView.password_entry.get()

    sql_username = "SELECT Username FROM User WHERE Username LIKE '%s'" % entryText

    sql_password = "SELECT Password FROM User WHERE Password LIKE '%s'" % entryText

    dbusername = str(querier.executeQuery(sql_username))
    dbusername = dbusername[3:-5]

    dbpassword = str(querier.executeQuery(sql_password))
    dbpassword = dbpassword[3: -5]

    if entryText == "":
        LoginView.error_text.config(text="Please enter your username first.", fg='red')
        return
    if entryText2 == "":
        LoginView.error_text.config(text="Please enter your password first.", fg='red')
        return
    if entryText != dbusername:
        LoginView.error_text.config(text="Username not correct.", fg='red')
        return
    if entryText2 != dbpassword:
        LoginView.error_text.config(text="Password not correct.", fg='red')
        return
    LoginView.screen.withdraw()
    NavigationView.navigationScreen.deiconify() # Durch diese Funktion wird der Main Bildschirm wieder aufgerufen


def client_ID_button():
    entry = BookingView.enterScooterIDLabel
    numberOfTuples = querier.executeQuery(
        str("SELECT COUNT(Booking_ID) AS Number FROM Booking WHERE Client_ID LIKE '%s'" % entry))
    print(numberOfTuples)


class BookingView:
    # everything that is used for the booking window gui is found here
    user = ""
    main_screen = Tk()
    main_screen.title("Booking-Overview")
    main_screen.geometry("630x305+480+200")
    # main_screen.state("zoomed") #Aktiviert den Vollbildmodus
    # main_screen.attributes('-alpha', 0.5) # Lässt di GUI transparent werden zu 50%
    main_screen.withdraw()  # Lässte den Main Bildschirm verschiwnden

    back_button = Button(main_screen, text="Back", padx=5, command=switchToNavigationView)
    back_button.grid(row=5, column=1)

    # Creating a Label Wdiget
    versionLabel = Label(main_screen, text="Version 1.0").grid(row=5, column=9)
    userGreetLabel = Label(main_screen, text="Welcome" + " " + user + " " + "!").grid(row=1, column=2)
    scooterIDLabel = Label(main_screen, text="Client ID").grid(row=2, column=3)
    enterScooterIDLabel = Entry(main_screen, text="Enter Client ID").grid(row=2, column=3, columnspan=4)
    ScooterIDButton = Button(main_screen, text="Enter", command=client_ID_button).grid(row=2, column=6)
    bookingsLabel = Label(main_screen, text="Booking [ID]").grid(row=3, column=2)
    dateLabel = Label(main_screen, text="Date").grid(row=3, column=3)
    timeLabel = Label(main_screen, text="Starttime").grid(row=3, column=4)
    durationLabel = Label(main_screen, text="Duration").grid(row=3, column=5)
    ScooterModelLabel = Label(main_screen, text="ScoModel").grid(row=3, column=6)
    priceLabel = Label(main_screen, text="Price").grid(row=3, column=7)
    ScooterLabel = Label(main_screen, text="Scooter [ID]").grid(row=3, column=8)

    # Listboxen
    bookingListbox = Listbox(main_screen)
    bookingListbox['height'] = 10
    bookingListbox['width'] = 10
    bookingListbox.grid(row=4, column=2)
    bookingListbox.insert(END, "Test")

    dateListbox = Listbox(main_screen)
    dateListbox['height'] = 10
    dateListbox['width'] = 10
    dateListbox.grid(row=4, column=3)
    dateListbox.insert(1)

    timeListbox = Listbox(main_screen)
    timeListbox['height'] = 10
    timeListbox['width'] = 10
    timeListbox.grid(row=4, column=4)
    timeListbox.insert(1)

    durationListbox = Listbox(main_screen)
    durationListbox['height'] = 10
    durationListbox['width'] = 10
    durationListbox.grid(row=4, column=5)
    durationListbox.insert(1)

    scooterModelListbox = Listbox(main_screen)
    scooterModelListbox['height'] = 10
    scooterModelListbox['width'] = 10
    scooterModelListbox.grid(row=4, column=6)
    scooterModelListbox.insert(1)

    priceListbox = Listbox(main_screen)
    priceListbox['height'] = 10
    priceListbox['width'] = 10
    priceListbox.grid(row=4, column=7)
    priceListbox.insert(1)

    userListbox = Listbox(main_screen)
    userListbox['height'] = 10
    userListbox['width'] = 10
    userListbox.grid(row=4, column=8)
    userListbox.insert(1)

    # Logo
    logo = Image.open('res/ScooTeq.png')
    # logo.resize((50,30), Image.ANTIALIAS)
    logo = ImageTk.PhotoImage(logo.resize((100, 67), Image.ANTIALIAS))
    logo_label = Label(main_screen, image=logo)
    logo_label.image = logo
    logo_label.grid(row=1, column=9)


class LoginView:
    # everything for the login window gui is created here
    screen = Toplevel(BookingView.main_screen)
    screen.title("Login-Window")
    screen.geometry("300x250+600+200")  # 300x250
    screen.attributes("-topmost", True)
    screen.resizable(0, 0)  # Damit der Bildschirm nicht maximiert werden kann
    # creation of labels and buttons
    Label(screen, text='Please enter your credentials down below!',
          bg="grey",
          width="300",
          height="2").pack()

    Label(screen, text="Username*").pack()
    username_entry = Entry(screen)
    username_entry.pack()
    username_entry.focus()

    Label(screen, text="Password*").pack()
    password_entry = Entry(screen, show="*")
    password_entry.pack()

    Label(screen, text="").pack()

    login_button = Button(screen, text="Login", command=button_action, padx=20)
    login_button.pack(ipadx=20)

    forgot_passsword = Button(screen, text="Forgot password", command=forgot_password, padx=5)
    forgot_passsword.pack(ipadx=6)

    exit_button = Button(screen, text="Exit", command=screen.quit, padx=5)
    exit_button.pack(ipadx=40.4)

    error_text = Label(screen)
    error_text.pack()


def searchForBooking():
    # this method will search the duration depending on the booking id you will enter
    bookingIDEntry = CalculationView.bookingIDEntry.get()
    # Datenbankabfrage
    cont.change_attributes('Booking', 'Duration', 'Booking_ID', bookingIDEntry)
    fetchedDuration = cont.controller_get_int()
    CalculationView.fetchedDurationLabel = Label(CalculationView.calculationScreen, text=fetchedDuration).grid(row=5, column=2)
    # Insert ins Calculationwindow
    BookingView.main_screen.update()
    # Datenbankabfrage
    cont.change_attributes('Booking', 'Price', 'Booking_ID', bookingIDEntry)
    # Kürzen der gefetchden Daten und übergeben ans Calculationwindow
    fetchedPrice = cont.controller_get_int()[9:-2]
    CalculationView.fetchedPriceInEuroLabel = Label(CalculationView.calculationScreen, text=fetchedPrice).grid(row=5,
                                                                                                               column=4)
    # Rechnung fürs Result
    result1 = round(float(fetchedDuration) * float(fetchedPrice), 2)
    CalculationView.fetchedResultsLabel = Label(CalculationView.calculationScreen, text=result1).grid(row=6, column=4)

class CalculationView:
    # here you find everything used for the calculation window gui
    calculationScreen = Toplevel(BookingView.main_screen)
    calculationScreen.title("Calculation")
    calculationScreen.attributes("-topmost", True)
    calculationScreen.geometry("380x160+550+200")
    calculationScreen.withdraw()

    fetchedDurationText = "-"
    fetchedPrice = "-"
    result = 0
    # Creation of Labels, entries and buttons
    userLabel = Label(calculationScreen, text="Hello user").grid(row=1, column=2)
    latestBookingsBtn = Button(calculationScreen, text="View latest bookings",
                               command=view_latest_bookings).grid(row=2, column=3)
    bookingIDLabel = Label(calculationScreen, text="Booking ID:").grid(row=3, column=2)
    bookingIDEntry = Entry(calculationScreen, text="please enter booking ID")
    bookingIDEntry.grid(row=3, column=3)
    enterBtn = Button(calculationScreen, text="Enter", command=searchForBooking).grid(row=3, column=4)
    durationLabel = Label(calculationScreen, text="Duration in min").grid(row=4, column=2)
    fetchedDurationLabel = Label(calculationScreen, text=fetchedDurationText).grid(row=5, column=2)
    priceInEuroLabel = Label(calculationScreen, text="Price in EUR/min").grid(row=4, column=4)
    fetchedPriceInEuroLabel = Label(calculationScreen, text=fetchedPrice).grid(row=5, column=4)
    xLabel1 = Label(calculationScreen, text="x").grid(row=4, column=3)
    xLabel2 = Label(calculationScreen, text="x").grid(row=5, column=3)
    resultsLabel = Label(calculationScreen, text="Result in EUR: ").grid(row=6, column=3)
    fetchedResultsLabel = Label(calculationScreen, text=result).grid(row=6, column=4)

    backBtn = Button(calculationScreen, text="Back", command=switchToNavigationView, padx=5).grid(row=7, column=1)
    inviLavel = Label(calculationScreen, text="    ", padx=5).grid(row=7, column=5)


class NavigationView:
    navigationScreen = Toplevel(BookingView.main_screen)
    navigationScreen.title("Navigation")
    navigationScreen.geometry("400x360+550+200")
    navigationScreen.attributes("-topmost", True)
    navigationScreen.withdraw()
    # creation of labels and buttons
    fillerLabel = Label(navigationScreen, width=310, bg="grey").pack()
    checkClientProfileBtn = Button(navigationScreen, text="Check Client Profile", width=20, pady=5).pack(fill=BOTH)
    fillerLabel1 = Label(navigationScreen, width=310, bg="grey").pack()
    calculationScreenBtn = Button(navigationScreen, text="Calculate Booking", command=switchToCalculationView,
                                  width=20, pady=5).pack(fill=BOTH)
    fillerLabel2 = Label(navigationScreen, width=310, bg="grey").pack()
    deployScooterBtn = Button(navigationScreen, text="Deploy Scooter", command=switchToScooterView, width=20,
                              pady=5).pack(fill=BOTH)
    fillerLabel3 = Label(navigationScreen, width=310, bg="grey").pack()
    bookingScreenBtn = Button(navigationScreen, text="Booking-Overview", command=switchToBookingView,
                              width=20, pady=5).pack(fill=BOTH)
    fillerLabel4 = Label(navigationScreen, width=310, bg="grey").pack()

    scooterOptionsBtn = Button(navigationScreen, text="Scooter Options", width=20, pady=5).pack(fill=BOTH)
    fillerLabel5 = Label(navigationScreen, width=310, bg="grey").pack()
    fillerLabel6 = Label(navigationScreen, text="").pack()
    backBtn = Button(navigationScreen, text="Back", command=switchToLoginView, width=20).pack()


class ScooterView:
    # Here the scooter window of our gui is created
    scooter_screen = Toplevel(BookingView.main_screen)
    scooter_screen.title("Deploy-Scooter")
    scooter_screen.geometry("400x260+550+200")
    scooter_screen.attributes("-topmost", True)
    scooter_screen.withdraw()
    # creation of labels, entries and buttons
    scooter_model_label = Label(scooter_screen, text="Scooter-Model: Lite, Standard or Power").pack()
    scooter_model_entry = Entry(scooter_screen)
    scooter_model_entry.pack()
    scooter_latitude_label = Label(scooter_screen, text="Scooter-Latitude").pack()
    scooter_latitude_entry = Entry(scooter_screen)
    scooter_latitude_entry.pack()
    scooter_longitude_label = Label(scooter_screen, text="Scooter-Longitude").pack()
    scooter_longitude_entry = Entry(scooter_screen)
    scooter_longitude_entry.pack()
    fillerLabel7 = Label(scooter_screen, text="").pack()

    deploy_button = Button(scooter_screen, text="Deploy",command=deploy_scooter).pack()
    fillerLabel8 = Label(scooter_screen, text="").pack()
    backBtn = Button(scooter_screen, text="Back", command=switchToNavigationView2, width=6).pack()

mainloop()

if __name__ == '__main__':
    print("Hello")
