from Model import *

model = Model()

class Controller(object):
    # Here we create the controller class which is used to communicate between the model and view class
    def __init__(self, table, coloumn, checkcoloumn, checkvalue):
        self.table = table
        self.coloumn = coloumn
        self.checkcoloumn = checkcoloumn
        self.checkvalue = checkvalue

    def controller_get_int(self):
        # returns an integer
        content = model.get_sql_int(self.table, self.coloumn, self.checkcoloumn, self.checkvalue)
        return content

    def controller_get_string(self):
        # returns a string
        content = model.get_sql_string(self.table, self.coloumn, self.checkcoloumn, self.checkvalue)
        return content

    def change_attributes(self, table, coloumn, checkcoloumn, checkvalue):
        # set the attributes in the right order
        self.table = table
        self.coloumn = coloumn
        self.checkcoloumn = checkcoloumn
        self.checkvalue = checkvalue
        return self.table, self.coloumn, self.checkcoloumn, self.checkvalue

 
    def controllerLogin(entryText):
        user = model.executeQuery(Model, "SELECT Username FROM User WHERE Username LIKE '%s'" % entryText)
        print(user)

 
 
if __name__ == '__main__':
    Controller = Controller('User', 'Username', 'Username', 'RoGu')
    Controller.controller_get_string()
