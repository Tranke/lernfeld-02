"""This Script was used to initialize the Database. Dont run again."""
from DBHelper import DBHelper
from classScooter import Scooter

querierDB = DBHelper()

#drop tables
tablelist = ("Booking", "Scooter", "ScooterModel", "Client")
for x in tablelist:
    sql_query_drop = "DROP TABLE IF EXISTS %s" % x
    querierDB.execute_query(sql_query_drop)

#create Tables 
createlist = (  "Client(\
                    Client_ID INT PRIMARY KEY NOT NULL auto_increment,\
                    Surname VARCHAR(30),\
                    Forename VARCHAR(30),\
                    Birthday DATE,\
                    Street VARCHAR(30),\
                    Postcode INT(5),\
                    City VARCHAR(20),\
                    Phone VARCHAR(20),\
                    Email VARCHAR(30))",
                "ScooterModel(\
                    Model_No VARCHAR(10) PRIMARY KEY,\
                    Speed_in_kmh INT(3),\
                    Price_in_eurm DECIMAL(5,2),\
                    Max_kg INT(3))",
               "Scooter(\
                    Scooter_ID INT PRIMARY KEY NOT NULL auto_increment,\
                    Model_No VARCHAR(10),\
                    Longitude VARCHAR(10),\
                    Latitude VARCHAR(10),\
                    Battery DECIMAL(3),\
                    FOREIGN KEY (Model_No) REFERENCES ScooterModel(Model_No))",
               "Booking(\
                    Booking_ID INT PRIMARY KEY NOT NULL auto_increment,\
                    Client_ID INT NOT NULL,\
                    Scooter_ID INT NOT NULL,\
                    Datetime_start Datetime,\
                    Datetime_end Datetime,\
                    Duration INT,\
                    Distance INT,\
                    Price DECIMAL(5,2),\
                    FOREIGN KEY (Client_ID) REFERENCES Client(Client_ID),\
                    FOREIGN KEY (Scooter_ID) REFERENCES Scooter(Scooter_ID))")
             
for y in createlist:
    sql_query_create = "CREATE TABLE %s" % y                
    querierDB.execute_query(sql_query_create)

#insert into Clients, Scooter
clientlist = {'Surname'     :{1 : 'Schmidt',            2 : 'Hoff',             3 : 'Mueller'},
              'Forename'    :{1 : 'Klaudia',            2 : 'Chris',            3 : 'Stefan'},
              'Birthday'    :{1 : '1990-01-03',         2 : '1998-06-02',       3 : '2000-11-11'},
              'Street'      :{1 : 'Neumannstr. 11',     2 : 'Hauptstr. 26',     3 : 'Gutstr. 18'},
              'Postcode'    :{1 : '22111',              2 : '22305',            3 : '22043'},
              'City'        :{1 : 'Hamburg',            2 : 'Hamburg',          3 : 'Hamburg'},
              'Phone'       :{1 : '01765666111',        2 : '015201560987',     3 : '017642267842'},
              'Email'       :{1 : 'K.S@gmail.de',       2 : 'Hoff.c@web.de',    3 : 'Stefan1111@yahoo.de'}
             }

scooterlist = {'Model_No'       :{1 : 'Lite',               2 : 'Standard',         3 : 'Power'},
               'Speed_in_kmh'   :{1 : '10',                 2 : '25',               3 : '40'},
               'Price_in_eurm'  :{1 : '0.12',               2 : '0.24',             3 : '0.36'},
               'Max_kg'         :{1 : '100',                2 : '120',              3 : '140'},
             }

list_value = ""
list_key = ""
list = scooterlist
for x in tablelist:
    if x != 'Scooter' and x != "Booking":
        print(x)
        if x == 'Client':
            list = clientlist 
        for index in range(1,4):
                for key in list:
                    value = list[key][index]
                    list_value = "%s'%s'," % (list_value,value)
                    list_key  = "%s%s," % (list_key,key)
                list_value = list_value[:-1]
                list_key = list_key[:-1]
                sql_query_insert = "INSERT INTO %s (%s) VALUES (%s) " % (x,list_key,list_value) 
                print(sql_query_insert)
                querierDB.execute_query(sql_query_insert)
                list_value = ""
                list_key = ""    

Scooter_ID_1 = Scooter('Lite', '10.000654', '53.550341')
Scooter_ID_2 = Scooter('Standard','10.000654', '53.550341')
Scooter_ID_3 = Scooter('Power','10.000654', '53.550341')

sql_query_insert_booking = "INSERT INTO Booking (Client_ID, Scooter_ID, Datetime_start, Datetime_end, Duration, Distance,Price) VALUES (1, 1, '2021-06-16 10:15:00', '2021-06-16 10:30:00',15, 7000, '0.12') "
querierDB.execute_query(sql_query_insert_booking)

sql_query_insert_booking = "INSERT INTO Booking (Client_ID, Scooter_ID, Datetime_start, Datetime_end, Duration, Distance,Price) VALUES (1, 2, '2021-06-18 10:40:00', '2021-06-18 10:50:00',10, 3000, '0.24') "
querierDB.execute_query(sql_query_insert_booking)

sql_query_insert_booking = "INSERT INTO Booking (Client_ID, Scooter_ID, Datetime_start, Datetime_end, Duration, Distance,Price) VALUES (1, 3, '2021-06-10 18:15:00', '2021-06-10 18:30:00',30, 12000, '0.36') "
querierDB.execute_query(sql_query_insert_booking)
