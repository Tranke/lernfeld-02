"""Scooter Class with attributes and methods"""

from DBHelper import DBHelper
from geopy.geocoders import Nominatim
import datetime
import itertools

class Scooter(object):
    # This is the scooter class, which we use to deploy them
    def __init__(self, model, longi, lati):
      
        self.model_No = model
        self.longitude = longi
        self.latitude = lati
        self.battery = 100
        querierScooter = DBHelper()
        sql_query_scooter = "INSERT INTO Scooter (Model_No, Longitude, Latitude, Battery)\
                            VALUES ('%s', %s, %s, %s)" % (self.model_No, self.longitude, self.latitude, self.battery)                  
        querierScooter.execute_query(sql_query_scooter)


    def get_location(self):
        # Return the longitude and latitude of a scooter
        geolocator = Nominatim(user_agent = "classScooter")
        city = "Hamburg"
        country = "Germany"
        loc = geolocator.geocode(city + "," + country)
        long = loc.longitude
        lat = loc.latitude 
        print (long, lat)
        return long, lat 
    
 

    def booking_start(self, scooter_ID, client_ID):
        # Insert a scooter into booking
        querierScooter = DBHelper()
        sql_query_scooter = "INSERT INTO Booking (scooter_ID, client_ID, Datetime_start)\
                            VALUES (%s, %s, %s, %s)" % (scooter_ID, client_ID, get_time())
        querierScooter.execute_query(sql_query_scooter)

    def booking_end(self, booking_ID,):
        # and update the time of the booking
        querierScooter = DBHelper()
        sql_query_scooter = "UPDATE Booking SET Datetime_end = %s\
                            WHERE Booking_ID = %s" % (get_time(), booking_ID)
        querierScooter.execute_query(sql_query_scooter)


# geolocator = Nominatim(user_agent = "classScooter")

# city = "Hamburg"
# country = "Germany"
# loc = geolocator.geocode(city + "," + country)
# long = loc.longitude
# lat = loc.latitude 
# print (long, lat)

# now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
# print (now)

# adress="Hamburg"
# location = geolocator.geocode(adress)
# print(location.address)
# print((location.latitude, location.longitude))

def get_time():
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print (now)
    return now