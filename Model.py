# Database
import pymysql

hostname = 'sql11.freemysqlhosting.net'
username = 'sql11416325'
password = '2G53HZ2YZu'
database = 'sql11416325'


class Model():
    # create the model which is used for databank queries
    def __init__(self):
        self.message = "Hi I can do SQL queries"

    # method to execute sql Statements
    def executeQuery(self, sql):
        with pymysql.connect(host=hostname,
                             user=username,
                             password=password,
                             database=database) as con:
            try:
                cursor = con.cursor()
                cursor.execute(sql)
                con.commit()
                content = cursor.fetchall()
                # print(content)
                return content
            except pymysql.connect.Error as error:
                print(error)
    
    def calculatePrice(self, ID):
        # this method is used to determinate the price
        DBSelect = Model()
        dur = ''
        scoot_ID = ''
        model_No = ''
        price = ''
        book_ID = ID
        # Select scooter
        sql_query_select_booking_scooter_ID = "SELECT Scooter_ID\
                                            FROM Booking WHERE Booking_ID = %s" % book_ID
        scoot_ID = str(DBSelect.executeQuery(sql_query_select_booking_scooter_ID)[0])
        scoot_ID = scoot_ID[1:-2]
        print(scoot_ID)
        # Select duration
        sql_query_select_booking_duration = "SELECT Duration\
                                            FROM Booking WHERE Booking_ID = %s" % book_ID
        dur = str(DBSelect.executeQuery(sql_query_select_booking_duration)[0])
        dur = dur[1:-2]
        print(dur)
        # Select scooter model
        sql_query_select_scooter_model_no = "SELECT Model_No\
                                            FROM Scooter WHERE Scooter_ID = %s" % scoot_ID
        model_No = str((DBSelect.executeQuery(sql_query_select_scooter_model_no)[0]))
        model_No = model_No[1:-2]
        print(model_No)
        # Select the price
        sql_query_select_scooter_price = "SELECT Price_in_eurm\
                                            FROM ScooterModel WHERE Model_No = %s" % model_No
        price = str((DBSelect.executeQuery(sql_query_select_scooter_price)[0]))
        price = price[10:-4]
        print(price)

        total = round(int(dur) * float(price), 2)
        return total
        
    def get_sql_string(self,table, coloumn, checkcoloumn,checkvalue):
            # this method returns a querie as a string
            try:
                with pymysql.connect(host=hostname, 
                                user=username,
                                password = password,
                                database = database) as con:
                    sql_select = "SELECT %s FROM %s WHERE %s = '%s'" % (coloumn, table, checkcoloumn, checkvalue)
                    cursor = con.cursor()
                    cursor.execute(sql_select)
                    con.commit()
                    content = str(cursor.fetchall())[3:-5]
                    return content
            except  pymysql.connect.Error as error:
                    print(error)

    def get_sql_int(self,table, coloumn, checkcoloumn,checkvalue):
                # this method returns a querie as a integer
                try:
                    with pymysql.connect(host=hostname, 
                                    user=username,
                                    password = password,
                                    database = database) as con:
                        sql_select = "SELECT %s FROM %s WHERE %s = '%s'" % (coloumn, table, checkcoloumn, checkvalue)
                        cursor = con.cursor()
                        cursor.execute(sql_select)
                        con.commit()
                        content = str(cursor.fetchall())[2:-4]
                        return content
                except  pymysql.connect.Error as error:
                        print(error)
