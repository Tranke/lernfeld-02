"""This Script was used to initialize the Database. Don't run again."""
from DBHelper import DBHelper


querier = DBHelper()

# drop tables
sql_query_drop = "DROP TABLE IF EXISTS User"
querier.execute_query(sql_query_drop)

# create Tables

sql_query_create = "CREATE TABLE User(\
                    User_ID INT PRIMARY KEY NOT NULL auto_increment,\
                    Surname VARCHAR(30),\
                    Forename VARCHAR(30),\
                    Username VARCHAR(30),\
                    Password VARCHAR(30),\
                    Function VARCHAR(30))"
querier.execute_query(sql_query_create)

# insert into User
userlist = {'Surname': {1: 'Johannsen', 2: 'Tran', 3: 'Gurvich'},
            'Forename': {1: 'Jonas', 2: 'Kenny', 3: 'Robert'},
            'Username': {1: 'JoJo', 2: 'KeTr', 3: 'RoGu'},
            'Password': {1: 'JoJo', 2: 'KeTr', 3: 'RoGu'},
            'Department': {1: 'CEO', 2: 'Sales', 3: 'Finance'},

            }

list_value = ""
list_key = ""
for index in range(1, 4):
        for key in userlist:
            value = userlist[key][index]
            list_value = "%s'%s'," % (list_value, value)
            list_key = "%s%s," % (list_key, key)
        list_value = list_value[:-1]
        list_key = list_key[:-1]
        sql_query_insert = "INSERT INTO User (%s) VALUES (%s) " % (list_key, list_value)
        print(sql_query_insert)
        querier.execute_query(sql_query_insert)
        list_value = ""
        list_key = ""
