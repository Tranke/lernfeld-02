# Skript zur Konsolenbedienung der Anwendung
from Controller import *
from Model import *
import subprocess
querier = Model()


#def functions
def get_login_username(username):
    username_sql = "SELECT Username FROM User WHERE Username LIKE '%s'" % username
    content = str(querier.executeQuery(username_sql))
    content = content[3:-5]
    return  content

def get_login_password(username):
    password_sql = "SELECT Username FROM User WHERE Username LIKE '%s'" % username
    content = str(querier.executeQuery(password_sql))
    content = content[3: -5]
    return  content

def menu_client_profile():
    client_ans = True
    while client_ans:    
        print("""
        Welcome to the client profile menu.
        Choose between:
        1. View client profile information
        2. Change client information
        3. Exit 
        """)
        client_ans = input("What would you like to do?")
        if client_ans == "1":
            print("\nClient informattion:")
        if client_ans == "2":
            print("\nChange client information:")
        if client_ans == "3":
            print("\nGoing back to menu:")
            break

def menu_calculate_booking():
    calc_ans = True
    while calc_ans:    
        print("""
        Welcome to the price calculation menu.
        Choose between:
        1. Show latest bookings
        2. Enter booking ID to calculate the price
        3. Exit 
        """)
        calc_ans = input("What would you like to do?")
        if calc_ans == "1":
            print("\nDisplaying list of recent bookings:")
            sql_get_bookings = "SELECT Booking_ID FROM Booking ORDER BY Booking_ID DESC LIMIT 10"
            result_list = str(querier.executeQuery(sql_get_bookings))
            print("Booking ids: %s" %result_list)
            print("\n Going back to price calculation menu..")
        if calc_ans == "2":
            booking_id_input = input("\nPlease enter the booking ID you want to calculate the price from:")
            dur = str(querier.executeQuery("Select Duration from Booking WHERE Booking_ID LIKE %s" %booking_id_input))[2:-4]
            print("\nRenting duration: %s" %dur)
            price_per_min = str(querier.executeQuery("SELECT Price_in_eurm FROM ScooterModel AS scomo\
					                                LEFT JOIN Scooter AS sco ON scomo.Model_No = sco.Model_No\
                                                    LEFT JOIN Booking AS bo ON sco.Scooter_ID = bo.Scooter_ID\
                                                    WHERE Booking_ID LIKE '%s'" % booking_id_input))[11:-6]
            print("Price per min: %s EUR" %price_per_min)
            print("Calculating price with [Duration x Price per min]")
            result = round(int(dur) * float(price_per_min),2)
            print("The total price of the booking [ID]%s is %s EUR" % (booking_id_input, result))
            input("press any button to go back to the menu")
            break 
        if calc_ans == "3":
            print("\nGoing back to menu:")
            break

def menu():
    ans=True
    while ans:
        print("""
        Menu:
        Choose between:
        1.Check client profile
        2.Calculate booking
        3.Deploy Scooter
        4.Bookings overview
        5.Scooter options
        6.Exit
        """)
        ans=input("What would you like to do? ")
        if ans=="1":
            print("\nOpening client profile menu..")
            menu_client_profile()
        elif ans=="2":
            print("\n Opening calculate menu...")
            menu_calculate_booking()
        elif ans=="3":
            print("\n Opening deploy menu...")
        elif ans=="4":
            print("\n Opening booking overview..")
        elif ans=="5":
            print("\n Opening Scooter menu...")
        elif ans=="6":
            print("\n Cya") 
            ans = None
        else:
            print("\n Not Valid Choice Try again")



def main():
#Welcome message
    print("Hello User, please login by entering your credentials")
    username_input = input("Enter your username:")
    password_input = input("Enter your password:")
    db_username = (get_login_username(username_input))
    db_password = (get_login_password(username_input))
    while username_input != db_username or password_input != db_password:
        print("Wrong login information. Please reenter your credentials. ")
        username_input = input("Enter your username:")
        password_input = input("Enter your password:")
        db_username = (get_login_username(username_input))
        db_password = (get_login_password(username_input))
    print("logged in succesfully. Welcome %s!" %db_username)
    menu()

main()