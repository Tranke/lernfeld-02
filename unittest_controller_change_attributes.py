"""Unittest für controller.change_attribues()"""

import unittest
from Controller import *

class TestChangeAttributes(unittest.TestCase):
    """Unittests für change_attributes(table, coloumn, checkcoloumn, checkvalue)"""
    def test_change_attributes(self):
        cont = Controller("","","","")
        table = 'Client'
        coloumn = 'Surname'
        checkcoloumn = 'Client_ID'
        checkvalue = '1'
        self.assertEqual(('Client', 'Surname', 'Client_ID', '1'), cont.change_attributes(table, coloumn, checkcoloumn,checkvalue))

if __name__ == '__main__':
    unittest.main()
